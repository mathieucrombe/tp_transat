<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mclasse;

class Cclasse extends Controller
{
    public function index()
    {
        $data['title'] = "Classements 2007";
        
        $model = new Mclasse();
        $data['result'] = $model ->getAll();
        $page['contenu'] = view('v_classe', $data);
        return view('Commun/v_template',$page);
    }

}




?>