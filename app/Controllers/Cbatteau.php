<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mbatteau;
use App\Models\Mclasse;
use App\Models\Mskipper;

class Cbatteau extends Controller
{
    public function index($prmId = null)
    {
        $data['title'] = "Liste bateaux";
        if ($prmId != null) {
            $model = new Mbatteau();
            $data['result']=$model->getBateau($prmId);
            $model =new Mclasse();
            $data['resultc']=$model->getAllwithID($prmId);
        }
        $page['contenu'] = view('Batteau/v_liste_batteau', $data);
        return view('Commun/v_template',$page, $data);
    }

    public function detail($prmId = null)
    {
        $data['title'] = "Detail bateaux";
        if ($prmId != null) {
            $model = new Mbatteau();
            $data['result']=$model->getDetail($prmId);
            $model = new Mskipper();
            $data['results']=$model->getDetail($prmId);
        }
        $page['contenu'] = view('Batteau/v_detail_batteau', $data);
        return view('Commun/v_template',$page, $data);
    }


}




?>