<article class="main-article">
    <h1>Espace Organisateurs</h1>
    <p>
        <strong>
            Déjà inscrit ? Entrez votre email et mot de passe pour vous connecter
        </strong>
    </p>
    <h2>Connexion</h2>
    <form method="POST" action="login_action.php" class="form-login">
        <label for="login">E-mail</label>
        <input type="email" name="email" placeholder="Saisissez votre e-mail..." required>
        <label for="password">Mot de passe</label>
        <input type="password" name="password" placeholder="Saisissez votre mot de passe..." required>
        <input type="submit" value="Se connecter">
    </form>
    <img src=<?php echo base_url("images/champagne.jpg") ?> alt="Des vainqueurs sabrent le champagne">
</article>