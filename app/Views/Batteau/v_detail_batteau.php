<article class="main-article">
    <?php foreach ($result as $row) { ?>
        <h1><?php echo $row['nomBateau']; ?></h1>
        <img class="img-bateau" src=<?php $img = $row['photo'];
                                    echo base_url("images/bateaux/$img") ?> alt=<?php echo $row['nomBateau']; ?>>
        <h2>Classe de bateau</h2>
        <p><?php echo $row['typeCoque']; ?> <?php echo $row['nomClasse']; ?></p>
        <h2>Classement à l'arrivée</h2>
        <p>Place : <?php echo $row['classementFinal']; ?></p>
        <?php
        function secondsToTime($seconds)
        {
            $dtDebut = new \DateTime('@0'); //@ = Unix Timestamp
            $dtFin = new \DateTime("@$seconds");
            return $dtDebut->diff($dtFin)->format('%a jours, %h heures, %i minutes and %s secondes.');
        }
        ?>
        <p>Durée de course : <?php echo secondsToTime($row['tempsCourse']) ?></p>
        <h2>Skipper</h2>
        <?php foreach ($results as $row) { ?>
            <p><?php echo $row['nomSkipper']; ?></p>
            <img class="img-skipper" src=<?php $imgs = $row['photo'];
                                            echo base_url("images/skippers/$imgs") ?> alt="<?php echo $row['nomSkipper']; ?>">
        <?php } ?>
    <?php } ?>
</article>