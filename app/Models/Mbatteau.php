<?php

namespace App\Models;

use CodeIgniter\Model;

class Mbatteau extends Model
{
    protected $table = 'bateau';
    protected $primaryKey = 'Id';
    protected $returnType = 'array';

    public function getBateau($prmId)
    {
        $requete = $this->select('*')
            ->where(['idClasse' => $prmId])
            ->orderBy('classementFinal', 'ASC');
        return $requete->findAll();
    }
    public function getDetail($prmId)
    {
        $requete = $this->select('*, TIME_TO_SEC(tempsCourse) as tempsCourse')
            ->join('classebateau', 'classebateau.idClasse = bateau.idClasse', 'left')
            ->where(['idBateau' => $prmId]);
        return $requete->findAll();
    }
}
