<?php 

namespace App\Models;

use CodeIgniter\Model;

class Mclasse extends Model
{
    protected $table = 'classebateau';
    protected $primaryKey = 'Id';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('*');
        return $requete->findAll();
    }
    public function getAllwithID($prmId)
    {
        $requete = $this->select('*')
        ->where(['idClasse' => $prmId]);
        return $requete->findAll();
    }
    // public function postClasse($prmTest)
    // {
    //     $this->allowedFields = ['tailleCoque', 'typeCoque', 'nomClasse'];
    //     $this->insert($prmTest);
    //     //ID du nouvel enregistrement retourné dans un tableau associatif 
    //     $retour= $this->insertID('idClasse');
    //     return $retour;
    // }

}



?>