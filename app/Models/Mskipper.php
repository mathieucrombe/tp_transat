<?php 

namespace App\Models;

use CodeIgniter\Model;

class Mskipper extends Model
{
    protected $table = 'skipper';
    protected $primaryKey = 'Id';
    protected $returnType = 'array';
    public function getDetail($prmId)
    {
        $requete = $this->select('*')
            ->where(['idBateau' => $prmId]);
        return $requete->findAll();
    }
}
